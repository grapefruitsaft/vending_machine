#include "widget.h"
#include "ui_widget.h"
#include <QMessageBox>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setButton();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::changeMoney(int n){
    money += n;
    ui->lcdNumber->display(money);
    setButton();
}

void Widget::setButton(){
    ui->pbCoffee->setEnabled(money >=100);
    ui->pbTea->setEnabled(money >= 150);
    ui->pbCoke->setEnabled(money>=200);
    ui->pbReset->setEnabled(money>0);
}

void Widget::on_pb10_clicked()
{
    changeMoney(10);
}

void Widget::on_pb100_clicked()
{
    changeMoney(100);
}

void Widget::on_pb50_clicked()
{
    changeMoney(50);
}

void Widget::on_pb500_clicked()
{
    changeMoney(500);
}

void Widget::on_pbCoffee_clicked()
{
    changeMoney(-100);
}

void Widget::on_pbTea_clicked()
{
    changeMoney(-150);
}

void Widget::on_pbCoke_clicked()
{
    changeMoney(-200);
}

void Widget::on_pbReset_clicked()
{
    int res = money;
    int c_500, c_100, c_50, c_10;
    c_500 = res / 500;
    res %= 500;
    c_100 = res / 100;
    res %= 100;
    c_50 = res / 50;
    res %= 50;
    c_10 = res / 10;
    money = 0;
    ui->lcdNumber->display(money);
    setButton();
    char buf[100];
    sprintf(buf, "500 : %d, 100 : %d, 50 : %d, 10 : %d", c_500, c_100, c_50, c_10);
    QMessageBox msg;
    msg.information(nullptr, "error", buf);
}
